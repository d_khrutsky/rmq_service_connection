from setuptools import setup

setup(name='rmq_connection',
      version='0.0.1',
      description='Base class for RMQ consumers/publishers',
      url='https://bitbucket.org/smartpeople/spine_microservices',
      author='Denis Khrutsky',
      author_email='d.khrutsky@speople.pro',
      license='MIT',
      packages=['rmq_connection'],
      install_requires=[
          'pika',
      ],
      scripts=[
          'scripts/rmq_connection_setup.py'
      ],
      zip_safe=False)
