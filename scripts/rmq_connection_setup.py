from shutil import copyfile
import os
import sys
import pkgutil

package = pkgutil.get_loader("rmq_connection")
path = os.path.dirname(package.path.rstrip('__init__.py'))

cur_dir = os.getcwd()
settings_from = path + '/settings.py'
settings_to = cur_dir + '/settings.py'
manage_from = path + '/manage.py'
manage_to = cur_dir + '/manage.py'

if os.path.isfile(settings_to):
    sys.stdout.write('File settings.py exists, appending...')
    f = open(settings_to, 'a')
    with open(settings_from) as f2:
        f.write('\n# RabbitMQ connection settings\n')
        f.write(f2.read())
    f.close()

else:
    copyfile(settings_from, settings_to)

if os.path.isfile(manage_to):
    sys.stdout.write('File manage.py exists, appending...')
    f = open(manage_to, 'a')
    with open(manage_from) as f2:
        f.write('\n# RabbitMQ connection management\n')
        f.write(f2.read())
    f.close()
else:
    copyfile(manage_from, manage_to)
