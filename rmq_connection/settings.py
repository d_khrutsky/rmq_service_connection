import os

hostname = os.environ.get('RMQ_PORT_5672_TCP_ADDR', 'localhost')
RMQ_USERNAME = 'guest'
RMQ_PASSWORD = 'guest'
AMQP_URL = 'amqp://{}:{}@{}:5672/%2F'.format(RMQ_USERNAME, RMQ_PASSWORD, hostname)
EXCHANGE_NAME = 'msg_exchange'
EXCHANGE_TYPE = 'topic'
QUEUE_NAME = 'messages'
