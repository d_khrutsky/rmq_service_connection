import pika
import logging
import sys
import json
from time import time


LOG_FORMAT = ('%(levelname) -10s %(asctime)s %(name) -30s %(funcName) '
              '-35s %(lineno) -5d: %(message)s')
LOGGER = logging.getLogger(__name__)

try:
    import settings
except ImportError:
    LOGGER.error('You must provide settings.py file for RabbitMQ connection')
    sys.exit()


class RabbitMQException(Exception):
    pass


class RabbitMQConnection(object):

    def __init__(self):
        amqp_url = settings.AMQP_URL
        exchange_name = settings.EXCHANGE_NAME
        exchange_type = settings.EXCHANGE_TYPE

        queue_name = self.__class__.__name__

        self._connection = None
        self.channel = None
        self._closing = False
        self._consumer_tag = None
        self._url = amqp_url

        self.exchange = exchange_name
        self.exchange_type = exchange_type
        self.exchange_durable = True
        self.queue = queue_name

    def connect(self):
        LOGGER.info('Connecting to %s', self._url)
        # Create BlockingConnection
        connection = pika.BlockingConnection(pika.URLParameters(self._url))
        self._connection = connection
        # Create channel
        self.channel = self._connection.channel()
        # Create exchange
        self.channel.exchange_declare(
            exchange=self.exchange,
            exchange_type=self.exchange_type,
            durable=self.exchange_durable)
        # Create queue
        self.channel.queue_declare(self.queue)
        return connection

    def close_connection(self, reply_code=0, reply_text="Normal shutdown"):
        LOGGER.info('Closing connection')
        self._connection.close(reply_code, reply_text)
        self.channel = None
        if self._closing:
            self._connection.close()
        else:
            LOGGER.warning('Connection closed, reopening in 5 seconds: (%s) %s',
                           reply_code, reply_text)
            self._connection.add_timeout(5, self.reconnect)

    def reconnect(self):
        if not self._closing:
            self._connection = self.connect()

    def stop(self):
        LOGGER.info('Stopping')
        self._closing = True
        self.close_connection()
        LOGGER.info('Stopped')

    def simple_log(self, level, message, routing_key):
        self.channel.publish(self.exchange, routing_key, level + ':' + message)


class RabbitMQConsumer(RabbitMQConnection):

    topics_to_consume = []  # Override to set your own routing_keys or use __init__

    def __init__(self, topics_to_consume=None):
        super().__init__()
        if topics_to_consume:
            self.topics_to_consume = topics_to_consume

    def on_message(self, channel, method_frame, properties, body):
        """Use this method to add logic to your service

        Do not change the signature of this method.
        channel.basic_ack(delivery_tag=method_frame.delivery_tag)
        """
        self.channel.basic_ack(delivery_tag=method_frame.delivery_tag)

    def start(self):
        try:
            self.channel.start_consuming()
        except (KeyboardInterrupt, SystemExit):
            self.stop()

    def connect(self):
        super().connect()
        for routing_key in self.topics_to_consume:
            self.channel.queue_bind(exchange=self.exchange,
                                    queue=self.queue,
                                    routing_key=routing_key)
        self.on_init()
        self.channel.basic_consume(self.on_message, self.queue)

    def on_init(self):
        """Override this method to add logic after successful RMQ connection.
        """
        pass

    def on_destroy(self):
        """Override this method to do cleanup before closing RMQ connection.
        """
        pass

    def stop(self):
        LOGGER.info('Stopping')
        self.on_destroy()
        self._closing = True
        self.stop_consuming()
        # self.close_connection()
        LOGGER.info('Stopped')

    def stop_consuming(self):
        if self.channel:
            self.channel.queue_delete(self.queue, if_unused=True, if_empty=True)
            self.channel.exchange_delete(self.exchange, if_unused=True)
            LOGGER.info('Sending a Basic.Cancel RPC command to RabbitMQ')
            self.channel.cancel()


class RabbitMQPublisher(object):

    def __init__(self, amqp_url, exchange, block_callback=None, unblock_callback=None):
        """
        callback_method (method) – Callback to call on Connection.Blocked,
        having the signature callback_method(pika.frame.Method),
        where the method frame’s method member is of type pika.spec.Connection.Blocked
        """
        super().__init__(amqp_url, exchange)
        if block_callback and callable(block_callback):
            self._connection.add_on_connection_blocked_callback(block_callback)
        if unblock_callback and callable(unblock_callback):
            self._connection.add_on_connection_unblocked_callback(unblock_callback)

    def publish(self, routing_key, body):
        self.channel.basic_publish(self.exchange, routing_key, body)


class RabbitMQLogHandler(logging.Handler):
    """
    Example:

    c = RabbitMQConsumer()
    c.connect()
    handler = RabbitMQLogHandler(c.channel, c.exchange, 'example')

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(handler)

    logger.debug('Example')
    """

    def __init__(self, channel, exchange, service_name=None):
        """
        Use existing channel and exchange.
        """
        super().__init__(logging.DEBUG)
        self.channel = channel
        self.exchange = exchange
        self.service_name = service_name

    def emit(self, record):
        routing_key = 'log.' + self.service_name if self.service_name and self.service_name != '' else 'log'
        log_message = {
            "source": self.service_name,
            "message": record.msg % record.args,
            "level": record.levelname,
            "pathname": record.pathname,
            "timestamp": str(int((time() + 0.5) * 1000))
        }
        self.channel.basic_publish(self.exchange, routing_key, json.dumps(log_message))


if __name__ == '__main__':
    class Logger(RabbitMQConsumer):

        def on_message(self, channel, method_frame, properties, body):
            print(" [x] %r:%r" % (method_frame.routing_key, body))

        def on_init(self):
            print('Logger initialized')

        def on_destroy(self):
            print('Logger closed')

    logger_service = Logger()
    logger_service.connect()
    LOGGER.info(' [*] Waiting for messages. To exit press CTRL+C')
    logger_service.start()
