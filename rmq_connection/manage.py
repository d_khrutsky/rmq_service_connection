import os
import glob
from rmq_connection import launch_services


def get_modules_in_dir(directory):
    py_files = glob.glob(directory + '*.py')
    py_files.remove(directory + os.path.basename(__file__))
    for i in range(len(py_files)):
        py_files[i] = py_files[i].replace(directory, '')
        py_files[i] = py_files[i].rstrip('.py')
    return py_files

if __name__ == '__main__':
    directory = os.path.dirname(os.path.realpath(__file__)) + '/'
    py_files = get_modules_in_dir(directory)
    if py_files:
        launch_services(py_files)
