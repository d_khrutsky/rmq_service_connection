import multiprocessing
from rmq_connection import RabbitMQConsumer, RabbitMQPublisher
from time import sleep


def services_in_module(module):
    md = module.__dict__
    return [
        md[c] for c in md if (
            isinstance(md[c], type) and md[c].__module__ == module.__name__ and is_RMQ_connection(md[c])
        )
    ]


def is_RMQ_connection(cls):
    RMQ_CLASSES = set([RabbitMQConsumer, RabbitMQPublisher])
    return bool(set(cls.__bases__) & RMQ_CLASSES)


def launch_services(modules):
    services = []
    jobs = []
    for module in modules:
        module = __import__(module)
        # try:
        #     module = __import__(module)
        # except ImportError:
        #     print('Cannot import ' + str(module))
        #     continue
        s_in_m = services_in_module(module)
        services.extend(s_in_m)
        print('Found services: {} in module {}'.format([x.__name__ for x in s_in_m], str(module)))

    for Service in services:
        print('Launching {}'.format(Service.__name__))
        service_instance = Service()
        service_instance.connect()
        job = multiprocessing.Process(target=service_instance.start)
        jobs.append(job)
        job.start()

    try:
        while True:
            sleep(100)
    except (KeyboardInterrupt, SystemExit):
        print('\n [*] Received KeyboardInterrupt, terminating services...')
        for job in jobs:
            job.terminate()


if __name__ == '__main__':
    import logger
    modulename = logger
    launch_services(modulename)
