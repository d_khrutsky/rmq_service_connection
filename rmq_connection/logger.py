from rmq_connection import RabbitMQConsumer


class Logger(RabbitMQConsumer):

    topics_to_consume = ['a.*.*', 'in.#']

    def on_message(self, channel, method_frame, properties, body):
        super().on_message(channel, method_frame, properties, body)
        print(" [x] %r:%r" % (method_frame.routing_key, body))

    def on_init(self):
        print(' [*] Waiting for messages. To exit press CTRL+C')

    def on_destroy(self):
        print('\n [*] Logger closed')


class WeDontNeedThis(object):
    pass

if __name__ == '__main__':
    topics = ['a.#', 'b.*']
    logger_service = Logger(topics)
    logger_service.connect()
    try:
        logger_service.start()
    except KeyboardInterrupt:
        logger_service.stop()
